# step-project-forkio 

https://stava976.github.io/

### Задание

Сверстать макет [макет](https://www.figma.com/file/9lLwBJciU4yjDZBSnqqXSS/Forkio?node-id=0%3A1)

### Группа РЕ35. Проект FORKIO. 
Состав:
Ставняцкий Валерий. @stava976
Чевжик Антон. @AntonChevzhyk (на старте перевелся в другую группу, проинформировал 23.08).

### Распределение заданий:

#### Задание №1 - Ставняцкий Валерий

- Сверстать шапку сайта с верхним меню (включая выпадающее меню при малом разрешении экрана).
- Сверстать секцию `People Are Talking About Fork`.

#### Задание №2 - Ставняцкий Валерий

- Сверстать блок `Revolutionary Editor`. Кнопки `watch`/`star`/`fork` на макете сделаны одним слоем, поэтому с макета не получится скачать иконки. Сами кнопки надо сделать, чтобы выглядели как [здесь](https://github.com/baxterthehacker/public-repo) справа вверху (оттуда же с помощью инспектора можно взять все SVG иконки и скачать используемые на гитхабе стили).
- Светстать секцию `Here is what you get`.
- Сверстать секцию `Fork Subscription Pricing`. В блоке с прайсингом третий элемент всегда будет "выделен" и будет больше других (то есть не по клику/ховеру, а статически).

### Сборщик:
```
devDependencies": {
    "browser-sync": "^2.27.5",
    "del": "^6.0.0",
    "gulp": "^4.0.2",
    "gulp-clean": "^0.4.0",
    "gulp-clean-css": "^4.3.0",
    "gulp-concat": "^2.6.1",
    "gulp-debug": "^4.0.0",
    "gulp-if": "^3.0.0",
    "gulp-load-plugins": "^2.0.7",
    "gulp-sass": "^4.1.0",
    "gulp-imagemin": "^7.1.0",
    "gulp-sourcemaps": "^3.0.0",
    "gulp-terser": "^2.0.1",
    "gulp-watch": "^5.0.1",
    "multipipe": "^4.0.0",
    "yargs": "^17.1.1"
  },
  "dependencies": {
    "reset-css": "^5.0.1"
  }
```


